# Skrimish

## Requires
* node v6.9.0+

## Setup
`npm install` from root directory

## Running
Run `npm start` from the root.  Then open `http://localhost:8080`
