import 'bootstrap/dist/css/bootstrap.min.css';
import {find} from 'lodash';
import {action, computed, observable} from 'mobx';
import {observer} from 'mobx-react';
import {Component, SyntheticEvent} from 'react';
import * as React from 'react';
import * as SocketIOClient from 'socket.io-client';

import './app.less';
import Chat from './chat';
import LobbyPhase from './lobby-phase';
import PlayPhase from './play-phase';
import SetupPhase from './setup-phase';

export
const enum Phase {
	LOBBY,
	PLAY,
	SETUP,
};
export
type Stack = string[];

export
class Player {
	@observable public host: boolean = false;
	@observable public lobbyId: string = '';
	public id: string;
	public username: string;
}

const PhaseTransition = {
	[Phase.LOBBY]() {
		return Phase.SETUP;
	},
	[Phase.SETUP]() {
		return Phase.PLAY;
	},
	[Phase.PLAY]() {
		return Phase.LOBBY;
	},
};

export
class AppStore {
	@observable public attackingPlayerTurnIndex: number = 0;
	@observable public peers: Player[] = [];
	@observable public player: Player;
	@observable public playOrder: string[] = [];
	@observable public phase: Phase = Phase.LOBBY;
	@observable public stacks: Stack[] = [];
	@computed get attackingPlayerId(): string {
		return this.playOrder[this.attackingPlayerTurnIndex];
	}
	@computed get attackingPlayer(): Player {
		const playerId = this.attackingPlayerId;

		if(this.player.id === playerId) {
			return this.player;
		} else {
			return this.getPeer(playerId);
		}
	}
	@computed get isMyTurn(): boolean {
		return this.attackingPlayerId === this.player.id;
	}
	constructor() {
		this.initPlayer();
		(window as any).appStore = this;
	}
	public getPeer(peerId: string) {
		return find(this.peers, (peer) => (peer).id === peerId);
	}
	@action public endPhase() {
		this.phase = PhaseTransition[this.phase]();
	}
	@action public initPlayer() {
		this.player = new Player();
	}
	@action public endTurn() {
		if(this.attackingPlayerTurnIndex === this.playOrder.length - 1) {
			this.attackingPlayerTurnIndex = 0;
		} else {
			this.attackingPlayerTurnIndex++;
		}
	}
}

@observer
export default
class App extends Component<any, any> {
	public store: AppStore;
	public socket: SocketIOClient.Emitter;

	constructor(props: {}) {
		super(props);

		this.store = new AppStore();
		this.socket = SocketIOClient(':8080', {transports: ['websocket'], upgrade: false});
		(window as any).socket = this.socket; // TODO Remove Debug
		this.socket.on('connect', () => console.log('Socket connected')); // tslint:disable-line
		this.socket.on('disconnect', () => console.log('Socket disconnected')); // tslint:disable-line
	}

	public render() {
		const {
			phase,
			player,
		} = this.store;

		return (
			<div className="app container">
				{phase === Phase.LOBBY && (
					<LobbyPhase
						socket={this.socket}
						appStore={this.store}
					/>
				)}
				{phase === Phase.SETUP && (
					<SetupPhase
						socket={this.socket}
						appStore={this.store}
					/>
				)}
				{phase === Phase.PLAY && (
					<PlayPhase
						appStore={this.store}
						socket={this.socket}
					/>
				)}
				{player.lobbyId && (
					<Chat socket={this.socket} username={player.username} />
				)}
			</div>
		);
	}
}
