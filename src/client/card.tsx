import * as classNames from 'classnames';
import {Component} from 'react';
import * as React from 'react';

type Props = {
	hidden?: boolean;
	selected?: boolean;
	type?: string;
	onClick?: (type: string, ev: React.MouseEvent<HTMLElement>) => void;
	onMouseEnter?: (type: string, ev: React.MouseEvent<HTMLElement>) => void;
	onMouseLeave?: (type: string, ev: React.MouseEvent<HTMLElement>) => void;
};

const CARD_UNIT = 30;
const NOOP = function() {}; // tslint:disable-line

export default
class Card extends Component<Props, any> {
	public static CARD_HEIGHT = CARD_UNIT * 3.5;
	public static CARD_WIDTH = CARD_UNIT * 2.5;

	constructor(props: Props) {
		super(props);
	}

	public render() {
		const {
			hidden,
			onClick = NOOP,
			onMouseEnter = NOOP,
			onMouseLeave = NOOP,
			selected,
			type,
		} = this.props;
		const classes = classNames({
			hidden,
			hover: !(onClick === NOOP && onMouseEnter === NOOP),
		});
		return (
			<div
				className={`playing-card ${classes}`}
				onClick={(ev) => onClick(type, ev)}
				onMouseEnter={(ev) => onMouseEnter(type, ev)}
				onMouseLeave={(ev) => onMouseLeave(type, ev)}
			>
				{!hidden && <div className="playing-card-top-left-type">{type}</div>}
				{!hidden && <div className="playing-card-bottom-right-type">{type}</div>}
				{selected && <div className="playing-card-highlighting"/>}
			</div>
		);
	}
}
