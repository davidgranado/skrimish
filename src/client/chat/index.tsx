import {action, observable} from 'mobx';
import {observer} from 'mobx-react';
import * as React from 'react';
import {Component, SyntheticEvent} from 'react';
import * as SocketIOClient from 'socket.io-client';

import './chat.less';
import ScrollWindow from './scroll-window';

type Message = {
	username: string;
	message: string;
	timestamp: number;
};
type Props = {
	socket: SocketIOClient.Emitter;
	username: string;
};
export {Message};

class ChatStore {
	public props: Props;
	@observable public messages: Message[] = [];
	@observable public newMessage: string = '';
	@observable public collapsed: boolean = true; // TODO Debug to start closed
}

@observer
export default
class Chat extends Component<Props, any> {
	public store: ChatStore;

	constructor(props: Props) {
		super(props);
		this.store = new ChatStore();
		props.socket.on('peer-broadcast-message', (data: any) => this.handleLogChatMessage(data));
		props.socket.on('peer-joined', (data: any) => this.handlePeerJoined(data));
		props.socket.on('peer-left', (data: any) => this.handlePeerLeft(data));
	}

	@action public handleLogChatMessage({username, message}: Message) {
		this.store.messages.push({
			message,
			username,
			timestamp: Date.now(),
		});
	}

	@action public handlePeerJoined({username}: {username: string}) {
		this.store.messages.push({
			message: `${username} has joined`,
			timestamp: Date.now(),
			username: '',
		});
	}

	@action public handlePeerLeft({username}: {username: string}) {
		this.store.messages.push({
			message: `${username} has disconnected`,
			timestamp: Date.now(),
			username: '',
		});
	}

	@action public handleUpdateMessage(newMessage: string) {
		this.store.newMessage = newMessage;
	}

	@action public handleSubmitMessage(ev: SyntheticEvent<any>) {
		ev.preventDefault();

		if(!this.store.newMessage) {
			return;
		}

		const message = {
			message: this.store.newMessage,
			username: this.props.username,
		};

		this.props.socket.emit('player-submit-message', message);
		message.username += ' (Me)';
		this.store.messages.push({
			message: this.store.newMessage,
			timestamp: Date.now(),
			username: message.username,
		});

		this.store.newMessage = '';
	}

	@action public handleToggleCollapse() {
		this.store.collapsed = !this.store.collapsed;
	}

	public render() {
		return (
			<div className="chat">
				<button
					type="button"
					className="btn btn-link btn-sm chat-toggle"
					onClick={() => this.handleToggleCollapse()}
				>
					{this.store.collapsed ? 'Show ' : 'Hide'} Chat
				</button>
				{!this.store.collapsed && (
					<div>
						<ScrollWindow messages={this.store.messages} />
						<div className="chat-message">
							<form onSubmit={(ev) => this.handleSubmitMessage(ev)}>
								<input
									className="form-control"
									id="chat-message"
									placeholder="Say Something"
									type="text"
									value={this.store.newMessage}
									autoComplete="off"
									onChange={(ev) => this.handleUpdateMessage((ev.target as HTMLInputElement).value)}
								/>
							</form>
						</div>
					</div>
				)}
			</div>
		);
	}
}
