import {observer} from 'mobx-react';
import * as React from 'react';
import {Component} from 'react';

import {Message} from './index';

type Props = {
	messages: Message[];
};

@observer
export default
class ScrollWindow extends Component<Props, any> {
	public props: Props;
	public el: HTMLElement;

	constructor(props: Props) {
		super(props);
	}

	public componentDidMount() {
		this.scrollToBottom();
	}

	public componentDidUpdate() {
		this.scrollToBottom();
	}

	public scrollToBottom() {
		const lastMessage = this.el.querySelector('.chat-log-message:last-child');

		if(lastMessage) {
			lastMessage.scrollIntoView();
		}
	}

	public render() {
		return (
			<div className="chat-log" ref={(el) => this.el = el}>
				{this.props.messages.map(({message, timestamp, username}) => {
					return (
						<div key={timestamp} className="chat-log-message">
							<strong>{`[${(new Date(timestamp)).toLocaleTimeString()}]${username}: `}</strong>
							<span className={username ? '' : 'chat-log-system-message'}>
								{message}
							</span>
						</div>
					);
				})}
			</div>
		);
	}
}
