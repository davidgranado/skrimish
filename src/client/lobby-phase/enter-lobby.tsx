import * as React from 'react';
import {Component, FormEvent, SyntheticEvent} from 'react';
import * as SocketIOClient from 'socket.io-client';

type Props = {
	socket: SocketIOClient.Emitter;
};
type ValidState = {
	lobbyId?: string;
	username?: string;
};
type ErrorState = {
	errMessage?: string;
};
type State = ValidState | ErrorState;

export default
class LobbyPhase extends Component<Props, State> {
	public props: Props;
	public state: State;

	constructor(props: Props) {
		super(props);

		this.state = {
			errMessage: '',
			lobbyId: '',
			username: '',
		};

		const socket = props.socket;
		socket.on('player-join-lobby-failed', (errMessage: string) => {
			this.setState({
				errMessage,
			});
		});
	}

	public componentWillUnmount() {
		this.props.socket.off('player-joined-lobby');
		this.props.socket.off('player-join-lobby-failed');
	}

	public handleUpdateLobbyId(ev: SyntheticEvent<HTMLInputElement>) {
		let el = ev.target;
		this.setState({
			lobbyId: (el as HTMLInputElement).value,
		});
	}

	public handleUpdateUsername(ev: SyntheticEvent<HTMLInputElement>) {
		let el = (ev.target as any);
		this.setState({
			username: el.value,
		});
	}

	public handleJoinLobby(ev: FormEvent<HTMLFormElement>) {
		let {lobbyId, username} = this.state as any;
		this.props.socket.emit('player-join-lobby', {lobbyId, username});
		ev.preventDefault();
	}

	public render() {
		const {
			lobbyId,
			username,
			errMessage,
		} = this.state as any;

		return (
			<form className="form" onSubmit={(ev) => this.handleJoinLobby(ev)}>
				<div className="form-group">
					<label htmlFor="lobby-id">Enter Lobby Id</label>
					<input
						className="form-control"
						id="lobby-id"
						placeholder="Lobby Name"
						type="text"
						value={lobbyId}
						onChange={(ev) => this.handleUpdateLobbyId(ev)}
					/>
				</div>
				<div className="form-group">
					<label htmlFor="username">Username</label>
					<input
						className="form-control"
						id="username"
						placeholder="Username"
						type="text"
						value={username}
						onChange={(ev) => this.handleUpdateUsername(ev)}
					/>
				</div>
				<button
					className="form-control btn btn-primary"
					type="submit"
					>
						Join
					</button>
				{errMessage && (
					<div>
						{errMessage}
					</div>
				)}
			</form>
		);
	}
}
