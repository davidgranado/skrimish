import {find} from 'lodash';
import {action, computed, observable} from 'mobx';
import {observer} from 'mobx-react';
import * as React from 'react';
import {Component} from 'react';
import * as SocketIOClient from 'socket.io-client';

import {AppStore, Player} from '../app';
import EnterLobby from './enter-lobby';
import './lobby-phase.less';

type Props = {
	socket: SocketIOClient.Emitter;
	appStore: AppStore;
};

class LobbyStore {
	@observable public ready: boolean = false;
	@observable public readyPeers: string[] = [];

	private appStore: AppStore;

	@computed get peers(): Player[] {
		return this.appStore.peers;
	}
	@computed get player(): Player {
		return this.appStore.player;
	}
	@computed get readyToLaunch(): boolean {
		return this.ready && this.readyPeers.length === this.peers.length;
	}
	@computed get joinedRoom() {
		return !!this.player.lobbyId;
	}
	constructor(appStore: AppStore) {
		this.appStore = appStore;
	}
	public endPhase() {
		this.appStore.endPhase();
	}
}

@observer
export default
class LobbyPhase extends Component<Props, any> {
	public props: Props;
	public store: LobbyStore;

	constructor(props: Props) {
		super(props);
		this.props.socket.on('setup-phase-starting', this.handleGameLaunching);
		this.props.socket.on('player-joined-lobby', this.handleJoinLobby);
		this.props.socket.on('peer-joined', this.handlePeerJoined);
		this.props.socket.on('peer-set-ready', this.handlePeerSetReady);
		this.props.socket.on('peer-left', (data: any) => this.handlePlayerLeft(data));

		this.store = new LobbyStore(props.appStore);
		(window as any).lobbyStore = this.store; // TODO Remove Debug
	}

	public componentWillUnmount() {
		this.props.socket.off('setup-phase-starting');
		this.props.socket.off('player-joined-lobby');
		this.props.socket.off('peer-joined');
		this.props.socket.off('peer-set-ready');
		this.props.socket.off('peer-left');
	}

	@action public handleGameLaunching = () => {
		this.store.endPhase();
	}

	@action public handleJoinLobby = (data: Player & {peers: Player[]}) => {
		const player = this.store.player;
		const peers = data.peers.map((peer, index) => {
			const newPeer = new Player();
			newPeer.id = data.peers[index].id;
			newPeer.lobbyId = data.peers[index].lobbyId;
			newPeer.username = data.peers[index].username;
			newPeer.host = data.peers[index].host;
			return newPeer;
		});

		player.id = data.id;
		player.lobbyId = data.lobbyId;
		player.username = data.username;
		player.host = data.host;
		(this.store.peers as any).replace(peers);
	}

	@action public handleLaunchGame() {
		this.props.socket.emit('start-setup-phase', {
			lobbyId: this.store.player.lobbyId,
		});
		this.store.endPhase();
	}

	@action public handlePeerJoined = (newPeer: Player) => {
		const peer: Player = new Player();
		peer.id = newPeer.id;
		peer.username = newPeer.username;
		peer.host = newPeer.host;
		this.store.peers.push(peer);
	}

	@action public handlePlayerLeft = ({peerId}: {peerId: string}) => {
		const peers = this.store.peers;
		(peers as any).replace(peers.filter((peer) => peer.id !== peerId));
	}

	@action public handlePeerSetReady = (data: Player) => {
		if(find(this.store.readyPeers, (playerId) => playerId === data.id)) {
			(this.store.readyPeers as any).remove(data.id);
		} else {
			this.store.readyPeers.push(data.id);
		}
	}

	@action public handleToggleReady = () => {
		this.props.socket.emit('player-set-ready', this.store.player);
		this.store.ready = !this.store.ready;
	}

	public peerIsReady(peerId: string) {
		return !!(~this.store.readyPeers.indexOf(peerId));
	}

	public render() {
		const player = this.store.player;
		const {
			host,
			lobbyId,
			username,
		} = player;
		const {
			ready,
			joinedRoom,
			peers,
			readyToLaunch,
		} = this.store;
		const readybuttonText = ready ? 'Ready' : 'Ready?';

		return (
			<div className="lobby-phase">
				{!joinedRoom && (
					<EnterLobby socket={this.props.socket} />
				) || (
					<div>
						<p>
							Lobby: {lobbyId} / Username {username} {host && '(Host)'}
						</p>
						<button
							className="btn btn-primary"
							type="button"
							onClick={this.handleToggleReady}
						>
							{readybuttonText}
						</button>{' '}
						{host && readyToLaunch && (
							<button
								className="btn btn-primary"
								type="button"
								onClick={() => this.handleLaunchGame()}
							>
								Launch
							</button>
						)}
						<br/>
						<ul>
							{peers.map((peer, index) => {
								return (
									<ul key={peer.id}>
										Opponent: {peer.username} {peer.host && '(Host)'} {this.peerIsReady(peer.id) && 'Ready'}
									</ul>
								);
							})}
						</ul>
					</div>
				)}
				<div className="video-container">
					<iframe
						frameBorder="0"
						height="360"
						id="ytplayer"
						src="https://www.youtube.com/embed/lJVv9IXfVoI"
						type="text/html"
						width="640"
					/>
				</div>
			</div>
		);
	}
}
