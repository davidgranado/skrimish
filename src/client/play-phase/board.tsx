import * as classNames from 'classnames';
import * as React from 'react';
import {BATTLE_RESULT} from './determine-winner';

const RESULT_LANGUAGE = {
	[BATTLE_RESULT.NULL]() {}, // tslint:disable-line
	[BATTLE_RESULT.ATTACKER_WINS](attackingPlayer: string, defendingPlayer: string) {
		return (
			<span>
				<strong>{attackingPlayer}</strong> won bout against <strong>{defendingPlayer}</strong>
			</span>
		);
	},
	[BATTLE_RESULT.ATTACKER_LOSES](attackingPlayer: string, defendingPlayer: string) {
		return (
			<span>
				<strong>{attackingPlayer}</strong> lost bout against <strong>{defendingPlayer}</strong>
			</span>
		);
	},
	[BATTLE_RESULT.STALEMATE](attackingPlayer: string, defendingPlayer: string) {
		return (
			<span>
				<strong>{attackingPlayer}</strong> and <strong>{defendingPlayer}</strong> had a stalemate
			</span>
		);
	},
	[BATTLE_RESULT.DOUBLE_KO](attackingPlayer: string, defendingPlayer: string) {
		return (
			<span>
				<strong>{attackingPlayer}</strong> and <strong>{defendingPlayer}</strong> both lost the bout
			</span>
		);
	},
	[BATTLE_RESULT.ATTACKER_WINS_GAME](attackingPlayer: string, defendingPlayer: string) {
		return (
			<span>
				<strong>{attackingPlayer}</strong> won the game
			</span>
		);
	},
	[BATTLE_RESULT.ATTACKER_LOSES_GAME](attackingPlayer: string, defendingPlayer: string) {
		return (
			<span>
				<strong>{defendingPlayer}</strong> won the game
			</span>
		);
	},
	[BATTLE_RESULT.DISCARD](attackingPlayer: string, defendingPlayer: string) {
		return (
			<span>
				<strong>{attackingPlayer}</strong> discarded a card
			</span>
		);
	},
};

type BoardParams = {
	attackingPlayer: string;
	battleResults: BATTLE_RESULT;
	children: Array<React.ComponentElement<any, any>> | React.ComponentElement<any, any>;
	defendingPlayer: string;
};

export default
function Board({attackingPlayer, battleResults, children, defendingPlayer}: BoardParams) {
	const resultsClasses = classNames('board-battle-results', {
		invisible: battleResults === -1,
	});
	return (
		<div className="board">
			{children}
			{(
				<div className={resultsClasses}>
					{(RESULT_LANGUAGE as any)[battleResults](attackingPlayer, defendingPlayer)}
				</div>
			)}
		</div>
	);
}
