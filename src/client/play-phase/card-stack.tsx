import {Component} from 'react';
import * as React from 'react';

import Card from '../card';

export default
class CardStack extends Component<any, any> {
	constructor(props: {}) {
		super(props);
	}

	public render() {
		return (
			<div className="card-stack">
				<Card hidden />
			</div>
		);
	}
}
