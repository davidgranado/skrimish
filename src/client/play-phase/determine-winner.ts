import {CardPlay} from './.';

export
const enum BATTLE_RESULT {
	ATTACKER_WINS,
	ATTACKER_WINS_GAME,
	ATTACKER_LOSES,
	ATTACKER_LOSES_GAME,
	DOUBLE_KO,
	DISCARD,
	NULL,
	STALEMATE,
};

function determineWinner(attackingCard: CardPlay, defendingCard: CardPlay): number {
	if(attackingCard.cardType === 'C') {
		if(defendingCard.cardType === 'C') {
			return BATTLE_RESULT.ATTACKER_WINS_GAME;
		} else {
			return BATTLE_RESULT.ATTACKER_LOSES_GAME;
		}
	} else if(defendingCard.cardType === 'C') {
		return BATTLE_RESULT.ATTACKER_WINS_GAME; // End of "Game Over" conditions
	} else if(attackingCard.cardType === 'A') {
		if(defendingCard.cardType === 'S') {
			return BATTLE_RESULT.STALEMATE;
		} else {
			return BATTLE_RESULT.ATTACKER_WINS;
		}
	} else if(defendingCard.cardType === 'S') {
		return BATTLE_RESULT.DOUBLE_KO;
	} else { // Attacker is a number
		if(+defendingCard.cardType) {
			if(+attackingCard.cardType > +defendingCard.cardType) {
				return BATTLE_RESULT.ATTACKER_WINS;
			} else if(+attackingCard.cardType < +defendingCard.cardType) {
				return BATTLE_RESULT.ATTACKER_LOSES;
			} else { // Attackers are equal
				return BATTLE_RESULT.DOUBLE_KO;
			}
		} else { // Defender is 'A'
			return BATTLE_RESULT.ATTACKER_WINS;
		}
	}
}

export default determineWinner;
