import {action, autorun, computed, observable} from 'mobx';
import {observer} from 'mobx-react';
import {Component} from 'react';
import * as React from 'react';
import * as SocketIOClient from 'socket.io-client';
import './play-phase.less';

import {AppStore, Phase, Player} from '../app';
import Board from './board';
import PlayStore, {CARD_PLAY_TYPE, CardPlay} from './play-store';
import PlayerField from './player-field';

type Props = {
	socket: SocketIOClient.Emitter;
	appStore: AppStore;
};

@observer
export default
class PlayPhase extends Component<Props, any> {
	public props: Props;
	public store: PlayStore;

	constructor(props: Props) {
		super(props);

		this.store = new PlayStore(props.appStore);
		this.store.setAwaitingTurn();
		this.props.socket.on('peer-play-card', (data: any) => this.handlePeerPlayCard(data));
	}
	public componentWillUnmount() {
		this.props.socket.off('peer-play-card');
	}
	@action public handleRevealCard(stackIndex: number) {
		this.store.revealedPlayerCardIndex = (stackIndex === this.store.revealedPlayerCardIndex ? -1 : stackIndex);
	}
	@action public handleHideCard() {
		this.store.revealedPlayerCardIndex = -1;
	}
	@action public handleSelectPeerCard(peerId: string, stackIndex: number) {
		if(this.store.selectedPlayerCard === 'S') {
			return;
		}
		const cardPlay = {
			cardType: this.store.selectedPlayerCard,
			isLastCard: this.store.stacks[stackIndex].length === 1,
			opponentId: peerId,
			opponentStackIndex: stackIndex,
			peerId: this.store.player.id,
			peerStackIndex: this.store.selectedPlayerCardIndex,
			type: CARD_PLAY_TYPE.ATTACK,
		};
		this.props.socket.emit('player-play-card', cardPlay);
		this.store.awaitingPlay = false;
		this.store.cardPlays.push(cardPlay);
	}
	@action public handlePeerPlayCard({
		cardType,
		isLastCard,
		opponentId,
		opponentStackIndex,
		peerId,
		peerStackIndex,
		type,
	}: CardPlay) {
		this.store.cardPlays.push({
			peerId,
			cardType,
			peerStackIndex,
			isLastCard,
			type,
		});

		if (this.store.player.id === opponentId && opponentStackIndex !== undefined) {
			const {
				stacks,
				player,
			} = this.store;
			const cardPlay = {
				cardType: stacks[opponentStackIndex][0],
				isLastCard: stacks[opponentStackIndex].length === 1,
				peerId: player.id,
				peerStackIndex: opponentStackIndex,
				type: CARD_PLAY_TYPE.DEFEND,
			};
			this.props.socket.emit('player-play-card', cardPlay);
			this.store.cardPlays.push(cardPlay);
		}
	}
	@action public handleSelectPlayerCard(stackIndex: number) {
		if(
			this.store.selectedPlayerCardIndex === stackIndex &&
			this.store.selectedPlayerCard !== 'C'
		) {
			const cardPlay = {
				cardType: '?',
				isLastCard: this.store.stacks[stackIndex].length === 1,
				peerId: this.store.player.id,
				peerStackIndex: this.store.selectedPlayerCardIndex,
				type: CARD_PLAY_TYPE.DISCARD,
			};
			this.props.socket.emit('player-play-card', cardPlay);
			this.store.awaitingPlay = false;
			this.store.cardPlays.push(cardPlay);
		} else {
			this.store.selectedPlayerCardIndex = stackIndex;
		}
	}
	public render() {
		const {
			amDefending,
			attackingPlayer,
			awaitingPlay,
			battleResults,
			defendingPlayer,
			emptyPeerStacksStore,
			isMyTurn,
			peers,
			player,
			selectedPlayerCardIndex,
		} = this.store;
		const playFieldParams: any = {
			emptyStacks: emptyPeerStacksStore[player.id],
			revealedCards: this.store.revealedCards,
			username: player.username,
		};
		const boardProps: any = {
			attackingPlayer: isMyTurn ? 'You' : attackingPlayer.username,
			battleResults,
			defendingPlayer: defendingPlayer && (amDefending ? 'you' : defendingPlayer.username),
		};

		if(awaitingPlay) {
			playFieldParams.onClick = (index: number) => this.handleSelectPlayerCard(index);
			playFieldParams.onMouseEnter = (index: number) => this.handleRevealCard(index);
			playFieldParams.onMouseLeave = () => this.handleHideCard();
		}

		return (
			<div className="play-phase">
				<Board {...boardProps}>
					<div className="player-indicator">
						{isMyTurn ? 'Your' : `${attackingPlayer.username}'s`} turn
					</div>
					<div>
						{peers.map((peer) => {
							const params: any = {
								emptyStacks: emptyPeerStacksStore[peer.id],
								revealedCards: this.store.getPeerRevealedCards(peer.id),
								username: peer.username,
							};

							if(awaitingPlay && ~selectedPlayerCardIndex) {
								params.onClick = (index: number) => this.handleSelectPeerCard(peer.id, index);
							}

							return (
								<div key={peer.id}>
									<PlayerField
										cardCount={5}
										{...params}
									/>
								</div>
							);
						})}
					</div>
					<div className="player-field">
						<PlayerField
							cardCount={5}
							{...playFieldParams}
						/>
					</div>
				</Board>
			</div>
		);
	}
}

export {CardPlay};
