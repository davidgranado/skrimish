import {action, autorun, computed, observable} from 'mobx';

import {AppStore, Phase, Player} from '../app';
import determineWinner, {BATTLE_RESULT} from './determine-winner';

export
const enum CARD_PLAY_TYPE {
	ATTACK,
	DEFEND,
	DISCARD,
};
export
type CardPlay = {
	cardType: string;
	isLastCard: boolean;
	opponentId?: string;
	opponentStackIndex?: number;
	peerId: string;
	peerStackIndex: number;
	type: CARD_PLAY_TYPE;
};
class EmptyPeerStacksStore {
	constructor(playerId: string, peers: Player[]) {
		(this as any)[playerId] = observable([]);
		peers.map((peer) => {
			(this as any)[peer.id] = observable([]);
		});
	}
}

export default
class PlayStore {
	@observable public awaitingPlay: boolean = false;
	@observable public revealedPlayerCardIndex: number = -1;
	@observable public selectedPlayerCardIndex: number = -1;
	@observable public selectedPeerCard: number = -1;
	@observable public cardPlays: CardPlay[] = [];
	public emptyPeerStacksStore: {[peerId: string]: number[]};
	public appStore: AppStore;
	@computed get amDefending(): boolean {
		const defendingPlayer = this.defendingPlayer;
		return !!defendingPlayer && defendingPlayer.id === this.player.id;
	}
	@computed get battleResults(): number {
		if(this.cardPlays.length && this.cardPlays[0].type === CARD_PLAY_TYPE.DISCARD) {
			return BATTLE_RESULT.DISCARD;
		} else if(this.cardPlays.length !== 2) {
			return BATTLE_RESULT.NULL;
		} else {
			return determineWinner(this.cardPlays[0], this.cardPlays[1]);
		}
	}
	@computed get defendingPlayer(): Player | null {
		if(this.cardPlays.length !== 2) {
			return null;
		}

		const peerId = this.cardPlays[1].peerId;

		if(this.player.id === peerId) {
			return this.player;
		} else {
			return this.appStore.getPeer(peerId);
		}
	}
	@computed get revealedCards() {
		const revealedCards = this.getPeerRevealedCards(this.player.id);

		if(this.revealedPlayerCardIndex !== -1) {
			revealedCards.push({
				index: this.revealedPlayerCardIndex,
				type: this.appStore.stacks[this.revealedPlayerCardIndex][0],
			});
		}

		if(this.selectedPlayerCardIndex !== -1) {
			revealedCards.push({
				index: this.selectedPlayerCardIndex,
				type: this.appStore.stacks[this.selectedPlayerCardIndex][0],
			});
		}

		return revealedCards;
	}
	get selectedPlayerCard() {
		const index = this.selectedPlayerCardIndex;
		return index === -1 ? '' : this.appStore.stacks[index][0];
	}
	get attackingPlayer() {
		return this.appStore.attackingPlayer;
	}
	get peers() {
		return this.appStore.peers;
	}
	get isMyTurn() {
		return this.appStore.isMyTurn;
	}
	get player() {
		return this.appStore.player;
	}
	get stacks() {
		return this.appStore.stacks;
	}

	constructor(appStore: AppStore) {
		this.appStore = appStore;
		autorun(() => {
			if(this.cardPlays.length === 2 ||  (this.cardPlays.length && this.cardPlays[0].type === CARD_PLAY_TYPE.DISCARD)) {
				setTimeout(() => this.endTurn(), 3000);
			}
		});

		this.emptyPeerStacksStore = (new EmptyPeerStacksStore(this.player.id, this.appStore.peers) as any);
		(window as any).playStore = this; // TODO remove debug
	}
	@action public setAwaitingTurn() {
		this.awaitingPlay = this.isMyTurn;
	}
	public getPeerRevealedCards(peerId: string) {
		const cards: Array<{index: number, type: string}> = [];

		this.cardPlays.forEach((cardPlay) => {
			if(cardPlay.peerId === peerId) {
				cards.push({
					index: cardPlay.peerStackIndex,
					type: cardPlay.cardType,
				});
			}
		});
		return cards;
	}
	@action public endTurn() {
		// TODO Summarize this messy logic
		const [attackerPlay, defenderPlay] = this.cardPlays;

		if(
			this.battleResults === BATTLE_RESULT.ATTACKER_WINS_GAME ||
			this.battleResults === BATTLE_RESULT.ATTACKER_LOSES_GAME
		) {
			setTimeout(() => this.appStore.endPhase(), 3000);
			return;
		}

		if(this.battleResults === BATTLE_RESULT.DISCARD) {
			this.stacks[attackerPlay.peerStackIndex].shift();

			if(attackerPlay.isLastCard) {
				this.emptyPeerStacksStore[attackerPlay.peerId].push(attackerPlay.peerStackIndex);
			}
		} else if(this.battleResults === BATTLE_RESULT.ATTACKER_WINS) {
			if(this.amDefending) {
				this.stacks[defenderPlay.peerStackIndex].shift();
			}

			if(defenderPlay.isLastCard) {
				this.emptyPeerStacksStore[defenderPlay.peerId].push(defenderPlay.peerStackIndex);
			}
		} else if(this.battleResults === BATTLE_RESULT.ATTACKER_LOSES) {
			if(this.isMyTurn) {
				this.stacks[attackerPlay.peerStackIndex].shift();
			}

			if(attackerPlay.isLastCard) {
				this.emptyPeerStacksStore[attackerPlay.peerId].push(attackerPlay.peerStackIndex);
			}
		} else if(this.battleResults === BATTLE_RESULT.DOUBLE_KO) {
			if(this.amDefending) {
				this.stacks[defenderPlay.peerStackIndex].shift();
			}

			if(this.isMyTurn) {
				this.stacks[attackerPlay.peerStackIndex].shift();
			}

			if(defenderPlay.isLastCard) {
				this.emptyPeerStacksStore[defenderPlay.peerId].push(defenderPlay.peerStackIndex);
			}

			if(attackerPlay.isLastCard) {
				this.emptyPeerStacksStore[attackerPlay.peerId].push(attackerPlay.peerStackIndex);
			}
		}

		(this.cardPlays as any).clear();
		this.selectedPlayerCardIndex = -1;
		this.revealedPlayerCardIndex = -1;
		this.appStore.endTurn();
		this.awaitingPlay = this.isMyTurn;
	}
}
