import {each, fill} from 'lodash';
import * as React from 'react';

import Card from '../card';

type PlayerCard = {
	type: string;
	index: number;
};
type Props = {
	cardCount: number;
	emptyStacks: number[];
	onClick?: (index: number) => void;
	onMouseEnter?: (index: number) => void;
	onMouseLeave?: (index: number) => void;
	revealedCards: PlayerCard[];
	username: string;
};

type EventTypes = {
	onClick?: (index: number) => void;
	onMouseEnter?: (index: number) => void;
	onMouseLeave?: () => void;
};
type CardRequiredParams = {
	key: number;
	hidden: boolean;
	type: string;
};
type CardParams = CardRequiredParams & EventTypes;

export default
function PlayerField({
		cardCount,
		emptyStacks,
		onClick,
		onMouseEnter,
		onMouseLeave,
		revealedCards,
		username,
	}: Props) {
	const cards: Array<string | null> = fill(Array(cardCount), '');
	const eventHandlers = {
		onClick,
		onMouseEnter,
		onMouseLeave,
	};

	emptyStacks.forEach((stackIndex) => {
		cards[stackIndex] = null;
	});
	revealedCards.forEach((card) => {
		cards[card.index] = card.type;
	});

	return (
		<div className="player-field">
			{cards.map((card, index) => {
				if(card === null) {
					return (
						<div className="empty-stack" key={index} />
					);
				}

				const params: CardParams = {
					hidden: !card,
					key: index,
					type: card,
				};

				each(eventHandlers, (handler, name: keyof EventTypes) => {
					if(handler) {
						params[name] = () => handler(index);
					}
				});

				return (
					<Card  {...(params as any)}/>
				);
			})}
			<div className="play-field-username">
				{username}
			</div>
		</div>
	);
}
