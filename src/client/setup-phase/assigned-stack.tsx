import {Component, MouseEvent} from 'react';
import * as React from 'react';

import Card from '../card';

type Props = {
	cards: string[];
	index: number;
	onClick?: (index: number) => void;
	onUnassign?: (index: number, cardIndex: number) => void;
};

export default
function AssignedStack({cards, onClick, index, onUnassign}: Props) {
	let handleClick;

	if(onClick) {
		handleClick = function() {
			if(onClick) {
				onClick(index);
			}
		};
	}

	return (
		<div className="assigned-stack" onClick={handleClick}>
			{cards.map(function(type, cardIndex) {
				const params: any = {
					type,
					key: `${cardIndex}.${type}`,
				};

				if(onClick) {
					params.onClick = function(clickedType: string, ev: MouseEvent<HTMLElement>) {
						ev.stopPropagation();

						if(onUnassign) {
							onUnassign(index, cardIndex);
						}
					};
				}

				return (
					<Card
						{...params}
					/>
				);
			})}
		</div>
	);
}
