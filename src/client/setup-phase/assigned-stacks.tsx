import * as React from 'react';

import AssignedStack from './assigned-stack';

type Props = {
	stacks: string[][];
	onAssign?: (index: number) => void;
	onUnassign?: (index: number, cardIndex: number) => void;
};

export default
function AssignedStacks({stacks, onAssign, onUnassign}: Props) {
	return (
		<div className="assigned-stacks">
			{stacks.map(function(stack, index) {
				return (
					<AssignedStack
						cards={stack}
						onClick={onAssign}
						onUnassign={onUnassign}
						index={index}
						key={index}
					/>
				);
			})}
		</div>
	);
}
