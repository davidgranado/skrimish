import {compact, find, some} from 'lodash';
import {action, computed, observable, toJS} from 'mobx';
import {observer} from 'mobx-react';
import {Component} from 'react';
import * as React from 'react';
import * as SocketIOClient from 'socket.io-client';

import {AppStore, Phase, Player, Stack} from '../app';
import AssignedStacks from './assigned-stacks';
import './setup-phase.less';
import UnassignedCards from './unassigned-cards';

type Props = {
	appStore: AppStore;
	socket: SocketIOClient.Emitter;
};

class SetupStore {
	@observable public ready: boolean = false;
	@observable public readyPeerIds: string[] = [];
	@observable public selectedUnassigned: string = '';
	@observable public unassignedCards: {[key: string]: number} = {
		1: 0, // 5
		2: 0, // 5
		3: 0, // 3
		4: 0, // 3
		5: 0, // 2
		6: 0, // 2
		A: 0, // 2
		C: 0, // 1
		S: 0, // 2
	};

	private appStore: AppStore;
	@computed get assignmentComplete(): boolean {
		return !some(this.unassignedCards);
	}
	@computed get readyPeers() {
		return compact(this.readyPeerIds.map((peerId): Player | null => {
			return find(this.peers, (player: Player) => {
				return peerId === player.id;
			}) || null;
		}));
	}
	@computed get setupPhaseComplete() {
		return this.ready && this.peers.length === this.readyPeerIds.length;
	}
	@computed get peers() {
		return this.appStore.peers;
	}
	@computed get player() {
		return this.appStore.player;
	}
	@computed get playOrder() {
		return this.appStore.playOrder;
	}
	@computed get stacks() {
		return this.appStore.stacks;
	}

	constructor(appStore: AppStore) {
		this.appStore = appStore;
	}
	public endPhase() {
		this.appStore.endPhase();
	}
}

@observer
export default
class SetupPhase extends Component<Props, any> {
	public props: Props;
	public store: SetupStore;

	constructor(props: Props) {
		super(props);

		this.store = new SetupStore(props.appStore);
		this.props.socket.on('peer-setup-complete', this.handlePeerSetupComplete);

		this.DEBUGprepopulateStacks(); // TODO Remove debug
		(window as any).setupStore = this.store; // TODO Remove debug
	}

	@action public DEBUGprepopulateStacks() {
		(this.store.stacks as any).replace([
			observable(['1', '1', '1', '1', '1']),
			observable(['2', '2', '2', '2', '2']),
			observable(['3', '3', '3', '4', '4']),
			observable(['4', '5', '5', '6', '6']),
			observable(['A', 'A', 'S', 'S', 'C']),
		]);
	}

	public componentWillUnmount() {
		this.props.socket.off('peer-setup-complete');
	}

	@action public assignCard = (stackIndex: number) => {
		let store = this.store;

		if(store.selectedUnassigned) {
			const stacks = this.store.stacks;
			const unassignedCards = this.store.unassignedCards;

			if(stacks[stackIndex].length === 5) {
				return;
			}

			stacks[stackIndex].push(store.selectedUnassigned);
			unassignedCards[store.selectedUnassigned] -= 1;

			if(unassignedCards[store.selectedUnassigned] === 0) {
				store.selectedUnassigned = '';
			}
		}
	}

	@action public handlePeerSetupComplete = (data: Player) => {
		find(this.store.peers, (peer) => {
			if(peer.id === data.id) {
				this.store.readyPeerIds.push(peer.id);
				this.store.playOrder.push(peer.id);

				return true;
			} else {
				return false;
			}
		});

		if(this.store.setupPhaseComplete) {
			this.store.endPhase();
		}
	}

	@action public handlePlayerSetupComplete() {
		this.store.ready = true;
		this.props.socket.emit('player-setup-complete');
		this.store.playOrder.push(this.store.player.id);

		if(this.store.setupPhaseComplete) {
			this.store.endPhase();
		}
	}

	@action public selectedUnassignedCard = (type: string) => {
		if(!this.store.unassignedCards[type]) {
			return;
		}

		this.store.selectedUnassigned = type === this.store.selectedUnassigned ? '' : type;
	}

	@action public unassignCard = (stackIndex: number, cardIndex: number) => {
		const stacks = this.store.stacks;
		const unassignedCards = this.store.unassignedCards;
		const cardType: any = stacks[stackIndex].splice(cardIndex, 1);
		unassignedCards[cardType]++;
	}

	public render() {
		const {
			assignmentComplete,
			peers,
			ready,
			readyPeers,
			selectedUnassigned,
			stacks,
			unassignedCards,
		} = this.store;
		const unassignedParams: any = {
			stacks,
		};

		if(!ready) {
			unassignedParams.onAssign = this.assignCard;
			unassignedParams.onUnassign = this.unassignCard;
		}

		return (
			<form className="setup-phase">
				<ul>
					{readyPeers.map((peer) => {
						return (
							<li key={peer.id}>
								{peer.username} Complete
							</li>
						);
					})}
				</ul>
				<AssignedStacks
					{...unassignedParams}
				/>
				{!ready && peers && (
					<UnassignedCards
						cards={toJS(unassignedCards)}
						selectedCard={selectedUnassigned}
						onCardClick={this.selectedUnassignedCard}
					/>
				)}
				{!ready && assignmentComplete && (
					<button
						className="form-control btn btn-primary"
						type="button"
						onClick={() => this.handlePlayerSetupComplete()}
					>
						Done
					</button>
				)}
			</form>
		);
	}
}
