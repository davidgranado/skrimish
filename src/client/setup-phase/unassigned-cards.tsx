import {map} from 'lodash';
import * as React from 'react';

import Card from '../card';

type Props = {
	cards: {[key: string]: number};
	onCardClick: (type: string) => void;
	selectedCard: string;
};

export default
function UnassignedCards({cards, onCardClick, selectedCard}: Props) {
	return (
		<div className="unassigned-cards">
			{map(cards, function(count, type) {
				return (
					<div className="unassigned-cards-content" key={type}>
						<Card
							type={type}
							onClick={onCardClick}
							selected={type === selectedCard}
						/> x {count}
					</div>
				);
			})}
		</div>
	);
}
