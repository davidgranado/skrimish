import * as Hapi from 'hapi';
import * as inert from 'inert';
import * as path from 'path';
import * as SocketIO from 'socket.io';
import {Routes} from './routes';

const server = new Hapi.Server();
server.connection({
	port: 8080,
});
const io = SocketIO(server.listener);

io.on('connection', function(socket) {
	socket.on('player-join-lobby', Routes.playerJoinLobby.bind(null, io, socket));
	socket.on('start-setup-phase', Routes.startSetupPhase.bind(null, socket));
	socket.on('player-set-ready', Routes.playerSetReady.bind(null, socket));
});

server.register(inert, function(err) {
	if (err) {
		throw err;
	}

	server.route({
		handler: {
			file: path.normalize(__dirname + '/../../build/client/index.html'),
		},
		method: 'GET',
		path: '/',
	});
});

server.start(function(err) {
	if (err) {
		throw err;
	}
	console.log(`Server running at: ${server.info.uri}`); // tslint:disable-line
});

server.route({
	handler: {
		directory: {
			index: false,
			path: path.normalize(__dirname + '/../client/'),
			redirectToSlash: true,
		},
	},
	method: 'GET',
	path: '/resources/{param*}',
});
