import {find, map} from 'lodash';
import * as SocketIO from 'socket.io';

type AppData = {
	host: boolean;
	username: string;
};
type Room = {
	sockets: {
		[id: string]: boolean,
	},
	length: number,
};
type UserData = {
	lobbyId: string;
	username: string;
};
type Socket = SocketIO.Socket & {appData: AppData};
type SocketServer = SocketIO.Server;

const MAX_PLAYERS = 4;

export
class Routes {
	public static disconnet(io: SocketServer, socket: Socket, lobbyId: string) {
		io.sockets.in(lobbyId).emit('peer-left', {
			peerId: socket.id,
			username: socket.appData.username,
		});
	}
	public static startSetupPhase(socket: Socket, data: UserData) {
		socket.to(data.lobbyId).emit('setup-phase-starting');
	}
	public static playerJoinLobby(io: SocketServer, socket: Socket, data: UserData) {
		let {lobbyId, username} = data;
		const playOrder: number[] = [];

		if(!lobbyId) {
			socket.emit('player-join-lobby-failed', 'Lobby name required');
			return;
		}

		if(!username) {
			socket.emit('player-join-lobby-failed', 'Username required');
			return;
		}

		let room: Room = io.sockets.adapter.rooms[lobbyId];

		if(!room) {
			room = [] as any;
		}

		const peers = map(room.sockets, function(val, socketId) {
			const peerSocket: Socket = (io.sockets.connected[socketId] as any);
			return {
				host: peerSocket.appData.host,
				id: peerSocket.id,
				username: peerSocket.appData.username,
			};
		});

		if(find(peers, (user) => user.username === username)) {
			socket.emit('player-join-lobby-failed', `Username "${username}" already in use. Select another username`);
			return;
		}

		if(room.length > MAX_PLAYERS) {
			socket.emit('lobby-full');
		} else {
			socket.appData = {
				host: !peers.length,
				username,
			};
			io.sockets.in(lobbyId).emit('peer-joined', {
				host: !peers.length,
				id: socket.id,
				username: socket.appData.username,
			});
			socket.join(lobbyId);
			socket.emit('player-joined-lobby', {
				host: !peers.length,
				id: socket.id,
				lobbyId,
				peers,
				ready: false,
				username,
			});
			socket.on('disconnect', Routes.disconnet.bind(null, io, socket, lobbyId));
			socket.on('player-setup-complete', Routes.playerSetupComplete.bind(null, socket, lobbyId, playOrder));
			socket.on('player-submit-message', Routes.playerSubmitMessage.bind(null, socket, lobbyId));
			socket.on('player-play-card', Routes.playerPlayCard.bind(null, socket, lobbyId));
		}
	}
	public static playerPlayCard(socket: Socket, lobbyId: string, data: any) {
		socket.to(lobbyId).emit('peer-play-card', data);
	}
	public static playerSetReady(socket: Socket, data: any) {
		socket.to(data.lobbyId).emit('peer-set-ready', data);
	}
	public static playerSetupComplete(socket: Socket, lobbyId: string, playOrder: string[]) {
		playOrder.push(socket.id);
		socket.to(lobbyId).emit('peer-setup-complete', {
			id: socket.id,
		});
	}
	public static playerSubmitMessage(socket: Socket, lobbyId: string, message: string) {
		socket.to(lobbyId).emit('peer-broadcast-message', message);
	}
}
